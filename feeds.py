#-*- encoding: utf-8 -*-
import re,os,urllib2,time,random
import BeautifulSoup
from xml.dom.minidom import Document
import django,os,json,re
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "btrans.settings")
django.setup()
from rbook.models import *
import feedparser
import datetime
import time
from rbook.translate import translateFile


def downloadPage(link):
    print "downloading=",link
    req = urllib2.Request(link, None, {
        'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0',
        'Cookie' : 'per24h=1; per24c=11; llang=enrui; wrexpire=0; 70=10; 71=10; __utma=253024271.583265383.1418221605.1418234634.1418239138.4; __utmz=253024271.1418221605.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmc=253024271; __utmb=253024271.7.10.1418239138; __utmt=1',
        })
    response = urllib2.urlopen(req)
    return response.read()
"""

cnt = Word.objects.filter(trans=None).count()
for w in Word.objects.filter(trans=None):
    print cnt
    cnt -= 1
    print w.word
    
    data = downloadPage("http://www.wordreference.com/enru/"+w.word)
    #data = downloadPage("http://www.webproxy.net/view?q=http%3A%2F%2Fwww.wordreference.com%2Fenru%2F"+w.word)
    soup = BeautifulSoup(data)
    dv = soup.find("div",{"id":"article"})
    if dv==None: 
        print "NO DATA"
        continue
    dv = str(dv).replace("<br/><div class='small1'>Collins Russian Dictionary 2nd Edition © HarperCollins Publishers 2000, 1997:</div><br/>","")
    w.trans = dv
    w.save()    
    #break
cnt = Word.objects.filter(one_word='').count()

for w in Word.objects.filter(one_word=''):
    print cnt
    cnt -= 1
    print w.word
    
    data = downloadPage("https://translate.google.ru/translate_a/single?client=t&sl=en&tl=ru&hl=ru&dt=bd&dt=ex&dt=ld&dt=md&dt=qc&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&prev=bh&ssel=0&tsel=0&tk=516284|101042&q="+w.word)
    print data
    m = re.search(r"\[\[\[\"(.*?)\",\"(.*?)\"\],",data)
    if m!=None:
        print "word=",w.word
        print "comp=",m.group(2).lower()
        try:
            if w.word==m.group(2).lower():
                w.one_word = m.group(1)
                print w.one_word
                w.save()
        except:
            print "ERROR"
    
    
    #data = downloadPage("http://www.webproxy.net/view?q=http%3A%2F%2Fwww.wordreference.com%2Fenru%2F"+w.word)
"""
"""
feeds = [
'http://feeds.bbci.co.uk/news/technology/rss.xml',
#'http://feeds.bbci.co.uk/news/world/rss.xml',
]
"""
#News.objects.all().delete()
for feed in Feed.objects.all():
    fd = feedparser.parse(feed.href)
    #print fd
    for d in fd.entries:
        #print d
        title = d.title
        text  = d.description
        href  = d.links[0].href        
        dt = datetime.datetime.fromtimestamp(time.mktime(d['updated_parsed']))
        if News.objects.filter(source=feed,title=title,href=href).count()>0: continue
        #print title,"dt=",dt
        data = downloadPage(href)
        soup = BeautifulSoup.BeautifulSoup(data)
        bs = soup.find('div',{"property":"articleBody"})
        #print "title=",title
        if bs==None:
            #soup = BeautifulSoup.BeautifulSoup(data)
            #print "main_article_text" in data            
            bs = soup.find("div",{"class":"main_article_text "})
            if bs==None:
                bs = soup.find("div",{"class":"story-body"})
                
        if bs==None: continue    #print "bs=",bs
        [s.extract() for s in bs('script')]
        
        n = News(source=feed,title=title,href=href,dt=dt,text=text,is_downloaded=True)
        n.save()
        os.mkdir(n.text_dir())
        f = file(n.source_file_name(),"wb")
        f.write(str(bs))
        f.close()
        
        translateFile(None,0,n.source_file_name(),n.trans_file_name())
        n.is_processed=True
        n.save()
        #break
    #break
