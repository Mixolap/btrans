﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:fb="http://www.gribuser.ru/xml/fictionbook/2.0">
	<xsl:output method="xml" encoding="UTF-8"/>

	<xsl:template name="preexisting_id">
		<xsl:variable name="i" select="@id"/>
		<xsl:if test="@id and //fb:a[@xlink:href=concat('#',$i)]"><a name="{@id}"/></xsl:if>
	</xsl:template>

	<!-- image - block -->
	<xsl:template match="fb:image">
				<cover>
					<xsl:choose>
						<xsl:when test="starts-with(@xlink:href,'#')">
							<xsl:variable name="bin" select="substring-after(@xlink:href,'#')"/>
							<xsl:attribute name="b64">
								<!-- <xsl:text>data:image/jpg;base64,</xsl:text> -->
								<xsl:value-of select="(/node()/node()[@id=$bin])"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="b64">
								<xsl:text></xsl:text>
							</xsl:attribute>						
							<xsl:attribute name="src"><xsl:value-of select="@xlink:href"/></xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</cover>
	</xsl:template>

	
	<!-- annotation -->
	<xsl:template name="annotation">
		<xsl:call-template name="preexisting_id"/>
		<xsl:apply-templates/>
	</xsl:template>
	
	<!-- description -->
	<xsl:template name="description">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- book-title -->
	<xsl:template name="book-title">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- author -->
	<xsl:template name="author">
		<author>
			<xsl:attribute name="first-name">
				<xsl:value-of select="fb:first-name"/>
			</xsl:attribute>
			<xsl:attribute name="middle-name">
				<xsl:value-of select="fb:middle-name"/>
			</xsl:attribute>
			<xsl:attribute name="last-name">
				<xsl:value-of select="fb:last-name"/>
			</xsl:attribute>
			<xsl:attribute name="uuid">
				<xsl:value-of select="fb:id"/>
			</xsl:attribute>
		</author>
	</xsl:template>
	
	<!-- genre -->
	<xsl:template name="genre">
		<genre>
			<xsl:attribute name="name">
				<xsl:apply-templates/>
			</xsl:attribute>
		</genre>
	</xsl:template>	
	
	<!-- translator -->
	<xsl:template name="translator">
		<translator>
			<xsl:attribute name="first-name">
				<xsl:value-of select="fb:first-name"/>
			</xsl:attribute>
			<xsl:attribute name="middle-name">
				<xsl:value-of select="fb:middle-name"/>
			</xsl:attribute>
			<xsl:attribute name="last-name">
				<xsl:value-of select="fb:last-name"/>
			</xsl:attribute>
			<xsl:attribute name="uuid">
				<xsl:value-of select="fb:id"/>
			</xsl:attribute>
		</translator>
	</xsl:template>	

	<!-- sequence -->
	<xsl:template name="sequence">
		<LI/>
		<xsl:value-of select="@name"/>
		<xsl:if test="@number">
			<xsl:text disable-output-escaping="no">,&#032;#</xsl:text>
			<xsl:value-of select="@number"/>
		</xsl:if>
		<xsl:if test="fb:sequence">
			<UL>
				<xsl:for-each select="fb:sequence">
					<xsl:call-template name="sequence"/>
				</xsl:for-each>
			</UL>
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="/*">	
	<document>
		<xsl:apply-templates select="fb:description/fb:title-info/fb:coverpage/fb:image"/>
		<title>
			<xsl:for-each select="fb:description/fb:title-info/fb:book-title">
				<xsl:call-template name="book-title"/>
			</xsl:for-each>		
		</title>
		<authors>		
			<xsl:for-each select="fb:description/fb:title-info/fb:author">
				<xsl:call-template name="author"/>
			</xsl:for-each>
		</authors>
		<genres>		
			<xsl:for-each select="fb:description/fb:title-info/fb:genre">
				<xsl:call-template name="genre"/>
			</xsl:for-each>
		</genres>
		<translators>		
			<xsl:for-each select="fb:description/fb:title-info/fb:translator">
				<xsl:call-template name="translator"/>
			</xsl:for-each>
		</translators>
		<annotation>
			<xsl:for-each select="fb:description/fb:title-info/fb:annotation">
					<xsl:call-template name="annotation"/>
			</xsl:for-each>
		</annotation>
	</document>		
	</xsl:template>
</xsl:stylesheet>