﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:fb="http://www.gribuser.ru/xml/fictionbook/2.0">
	<xsl:output method="html" encoding="UTF-8"/>

	<xsl:template name="preexisting_id">
		<xsl:variable name="i" select="@id"/>
		<xsl:if test="@id and //fb:a[@xlink:href=concat('#',$i)]"><a name="{@id}"/></xsl:if>
	</xsl:template>

	<!-- image - block -->
	<xsl:template match="fb:image">
		<div align="center">
			<xsl:attribute name="style">
			  <xsl:text>text-align:center;</xsl:text>
			</xsl:attribute>
			<a>
				<xsl:attribute name="href">
					<xsl:text disable-output-escaping="no">{% url ereader file.id %}</xsl:text>
				</xsl:attribute>

				<img border="1">
					<xsl:choose>
						<xsl:when test="starts-with(@xlink:href,'#')">
							<xsl:variable name="bin" select="substring-after(@xlink:href,'#')"/>
							<xsl:attribute name="src">
								<xsl:text>data:image/jpg;base64,</xsl:text>
								<xsl:value-of select="(/node()/node()[@id=$bin])"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="src"><xsl:value-of select="@xlink:href"/></xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</img>
			</a>
		</div>
	</xsl:template>

	
	<!-- annotation -->
	<xsl:template name="annotation">
		<xsl:call-template name="preexisting_id"/>
		<h2>Аннотация</h2>
		<xsl:apply-templates/>
	</xsl:template>
	
	<!-- description -->
	<xsl:template name="description">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- author -->
	<xsl:template name="author">
		<xsl:text disable-output-escaping="no">Автор:&#032;</xsl:text>
		<xsl:value-of select="fb:first-name"/>
		<xsl:text disable-output-escaping="no">&#032;</xsl:text>
		<xsl:value-of select="fb:middle-name"/>&#032;
        <xsl:text disable-output-escaping="no">&#032;</xsl:text>
		<xsl:value-of select="fb:last-name"/>
		<xsl:text disable-output-escaping="yes">&lt;br&gt;</xsl:text>
	</xsl:template>
	
	<!-- genre -->
	<xsl:template name="genre">
		<xsl:text disable-output-escaping="no">Жанр:&#032;</xsl:text>
		<xsl:text disable-output-escaping="no">&#032;</xsl:text>		
		<xsl:apply-templates/>
		<xsl:text disable-output-escaping="yes">&lt;br&gt;</xsl:text>
	</xsl:template>	
	
	<!-- translator -->
	<xsl:template name="translator">
		<xsl:text disable-output-escaping="no">Перевод:&#032;</xsl:text>
		<xsl:value-of select="fb:first-name"/>
		<xsl:text disable-output-escaping="no">&#032;</xsl:text>
		<xsl:value-of select="fb:middle-name"/>&#032;
        <xsl:text disable-output-escaping="no">&#032;</xsl:text>
		<xsl:value-of select="fb:last-name"/>
		<xsl:text disable-output-escaping="yes">&lt;br&gt;</xsl:text>
	</xsl:template>	

	<!-- sequence -->
	<xsl:template name="sequence">
		<LI/>
		<xsl:value-of select="@name"/>
		<xsl:if test="@number">
			<xsl:text disable-output-escaping="no">,&#032;#</xsl:text>
			<xsl:value-of select="@number"/>
		</xsl:if>
		<xsl:if test="fb:sequence">
			<UL>
				<xsl:for-each select="fb:sequence">
					<xsl:call-template name="sequence"/>
				</xsl:for-each>
			</UL>
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="/*">
		<xsl:apply-templates select="fb:description/fb:title-info/fb:coverpage/fb:image"/>
		<h1>
			<a>
				<xsl:attribute name="href">
					<xsl:text disable-output-escaping="no">{% url ereader file.id %}</xsl:text>
				</xsl:attribute>			
				
				<xsl:attribute name="style">
					<xsl:text disable-output-escaping="no">text-decoration:none; color: #555555;</xsl:text>
				</xsl:attribute>			
				
			
				<xsl:apply-templates select="fb:description/fb:title-info/fb:book-title"/>
			</a>			
		</h1>			
		<h2>
			<small>
				<xsl:for-each select="fb:description/fb:title-info/fb:author">
						<b>
							<xsl:call-template name="author"/>
						</b>
				</xsl:for-each>
			</small>
		</h2>
		<h2>
			<small>
				<xsl:for-each select="fb:description/fb:title-info/fb:genre">
						<b>
							<xsl:call-template name="genre"/>
						</b>
				</xsl:for-each>
			</small>
		</h2>		
		<h2>
			<small>
				<xsl:for-each select="fb:description/fb:title-info/fb:translator">
						<b>
							<xsl:call-template name="translator"/>
						</b>
				</xsl:for-each>
			</small>
		</h2>		
		<xsl:if test="fb:description/fb:title-info/fb:sequence">
			<p>
				<xsl:for-each select="fb:description/fb:title-info/fb:sequence">
					<xsl:call-template name="sequence"/><xsl:text disable-output-escaping="yes">&lt;br&gt;</xsl:text>
				</xsl:for-each>
			</p>
		</xsl:if>
		<xsl:for-each select="fb:description/fb:title-info/fb:annotation">
			<div>
				<xsl:call-template name="annotation"/>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>