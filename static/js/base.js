
function recalcMenu()
{
    var total = ""
    var total_price = 0
    $(".select_menu").each(function(){
        var id = $(this).attr("rel")
    
        if($(this).attr("checked")=="checked"){
            var cnt = $(".menu-value[rel="+id+"]").val()            
            var price = cnt*parseInt($(".menu_price[rel="+id+"]").val())            
            total_price += price
            $(".total_menu[rel="+id+"]").html(price+"&nbsp;р.")            
            var name = $(this).attr("data-name")
            total += "<tr><td>"+name+"<input type='hidden' name='oid' value='"+id+"'/><input type='hidden' name='cnt' value='"+cnt+"'/></td><td>"+cnt+"</td><td class='tar'>"+price+"&nbsp;р.</td></tr>"
            
        }else{
            $(".menu-value[rel="+id+"]").val("0")
            $(".total_menu[rel="+id+"]").html("")            
        }
    })
    total += "<tr><td colspan='2'>Итого:</td><td class='tar'><b>"+total_price+"&nbsp;р.</b></td>"
    $(".your-menu").html(total)
}

$(function(){
    $(".next-days").live('click',function(){
            $(this).hide()
            $(".loading").removeClass("dn")
            $.get($(this).attr("href"),function(data){
                $(".loading").remove()
                $(".calendar").append($(data))            
            })
            return false;
    })
    
    
    
    
    $(".select_menu").click(function(){
        var id = $(this).attr("rel")
        if($(this).attr("checked")=="checked"){
            var man_cnt = parseInt($(".man_cnt").text())
            var per_man = parseInt($(this).attr("data-mans"))
            var cnt = man_cnt/per_man
            if(cnt-Math.floor(cnt)>0) cnt=Math.floor(cnt)+1
            $(".menu-value[rel="+id+"]").val(cnt)
        }else{
            $(".menu-value[rel="+id+"]").val("0")
        }            
        recalcMenu()
    })
    
    
    $(".add-menu").click(function(){
        var id = $(this).attr("rel")
        $(".menu-value[rel="+id+"]").val(parseInt($(".menu-value[rel="+id+"]").val())+1)
        recalcMenu()              
    })

    $(".rem-menu").click(function(){
        var id = $(this).attr("rel")
        if(parseInt($(".menu-value[rel="+id+"]").val())>0)
            $(".menu-value[rel="+id+"]").val(parseInt($(".menu-value[rel="+id+"]").val())-1)
        recalcMenu()              
    })
    
    recalcMenu()
})
