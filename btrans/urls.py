from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'rbook.views.home', name='home'),
    url(r'^login/$', 'rbook.views.login_user', name='login'),            
    url(r'^accounts/login/$', 'rbook.views.login_user', name='login'),            
    url(r'^register/$', 'rbook.views.register', name='register'),    
    
    url(r'^upload/$', 'rbook.views.upload_book', name='upload'),        
    url(r'^learn/$', 'rbook.views.learn', name='learn'),        
    url(r'^learn/(?P<bid>\d+)/(?P<chapter>\d+)/$', 'rbook.views.learn', name='learn_from'),        
    url(r'^translate/$', 'rbook.views.translate', name='translate'),        
    
    
    url(r'^bookmark/remove/(?P<id>\d+)/$', 'rbook.views.removeBookmark', name='remove_bookmark'),        
    
    url(r'^book/(?P<uid>\w+)/$', 'rbook.views.book', name='book'),     
    url(r'^book/(?P<uid>\w+)/(?P<page>\d+)/$', 'rbook.views.book', name='book'),     
    url(r'^book/translation/(?P<uid>\w+)/(?P<page>\d+)/$', 'rbook.views.bookTranslation', name='book_translation'),     
    url(r'^book/(?P<w1>.+)/(?P<w2>.+)/$', 'rbook.views.checkWord', name='check'),     
    
    url(r'^books/user/(?P<uid>\w+)/$', 'rbook.views.booksUser', name='books_user'),     
    url(r'^books/genre/(?P<uid>\w+)/$', 'rbook.views.booksGenre', name='books_genre'),     
    url(r'^books/author/(?P<uid>\w+)/$', 'rbook.views.booksAuthor', name='books_author'),     
    url(r'^books/series/(?P<uid>\w+)/$', 'rbook.views.booksSeries', name='books_series'),     
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^news/$', 'rbook.views.news', name='news'),        
    url(r'^news/(?P<id>\d+)/$', 'rbook.views.readTopic', name='read_topic'),        
    
    url(r'^admin/', include(admin.site.urls)),
)
