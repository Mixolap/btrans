from rbook.models import *
from django.conf import settings

def vars(request):
    if request.user.is_authenticated():
        ln = Learn.objects.filter(user=request.user,acnt__lt=4).order_by("-id")[:10]
        words_count = Learn.objects.filter(user=request.user,acnt__gte=4).count()
        learn_count = Learn.objects.filter(user=request.user,acnt__lt=4).count()
        learned = Learn.objects.filter(user=request.user,acnt=4).order_by("-dt")[:10]
        
    genre = Genre.objects.all()
    authors = Author.objects.all()
    series = Series.objects.all()
    if request.user.is_authenticated():
        bookmarks = LastRead.objects.filter(user=request.user).order_by("-dt")
    
    return locals()