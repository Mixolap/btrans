# -*- encoding: utf-8 -*-

import BeautifulSoup
#from django.template.defaultfilters import escape_filter
import cgi
def parseFB2(book):
    infile = BeautifulSoup(file(book.book_file_fb2(),"r"))
    
    img    = ""
    book_title  = infile.find("book-title").getText()
    author = infile.find("author").getText()
    
    imgs = infile.find_all("binary")
    
    if len(imgs)>0:
        img = imgs[0].getText()
    
    if img!="":
        ifile = file(book.img_path(),"w")
        ifile.write(img.decode('base64'))
        ifile.close()
        img = book.img_url()
    
    pnum = 1
    sections = []
     
    ss = infile.find_all("section")
    if len(ss)>0 and len(ss[0].find_all("section"))>0: ss = ss[0].find_all("section")
        
    for s in ss:        
        title = ''
        for d in s.find("title").find_all("p"):
            title += "%s"%d.getText()            
            title += ". "
        sections.append([pnum,title,s.find_all("p")])
        pnum += 1
    
    
    # Запись оглавления
    html = "<div>"
    html += "<img src='%s'/>"%img
    html += "<h1>%s</h1>"%book_title
    html += "<h2>%s</h2>"%author
    html += "<ul>"
    xnum = 1
    for d in sections:
        html += "<li><a href='%s'>%s</a></li>"%(book.get_page_url(xnum),d[1])
        xnum += 1
    html += "</ul></div>"
    
    outfile = file(book.book_file_htm(0),"w")
    outfile.write(html.encode('utf-8'))
    outfile.close()
    
    # Запись глав    
    pnum = 1
    
    
    for s in sections:
        html = "<div>"
        html += "<h1>%s</h1>"%s[1]
        for d in s[2]: 
            if d.parent and d.parent.name=="title":continue
            html += "<p>%s</p>"%cgi.escape(d.getText()).encode('ascii', 'xmlcharrefreplace')
        html += "</div>"
        
        outfile = file(book.book_file_htm(pnum),"w")
        outfile.write(html.encode('utf-8'))
        outfile.close()
        pnum += 1
        
        #print html
        #break
        
    
    
    
    
    
    

