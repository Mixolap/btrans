#-*- encoding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect,HttpResponse,Http404
from django.shortcuts import render,render_to_response
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.template import loader, Context
from btrans.send_signal import send_signal
from models import *
#from fb2 import makeHTM,parsePerPages
from parse_fb2 import parseFB2
from translate import translateFile
import os,random,datetime

def catch_ulogin_signal(*args, **kwargs):
    """
    Обновляет модель пользователя: исправляет username, имя и фамилию на
    полученные от провайдера.

    В реальной жизни следует иметь в виду, что username должен быть уникальным,
    а в социальной сети может быть много "тёзок" и, как следствие,
    возможно нарушение уникальности.
    """
    
    user=kwargs['user']
    json=kwargs['ulogin_data']
    if kwargs['registered']:
        user.first_name=json['first_name']
        user.last_name=json['last_name']
        user.save()
try:        
    from django_ulogin.models import ULoginUser
    from django_ulogin.signals import assign
    assign.connect(receiver = catch_ulogin_signal,
                   sender   = ULoginUser,
                   dispatch_uid = 'customize.models')
except:
    print "NO ULOGIN"


def login_user(request,template_name="login.html"):
    if isMobile(request): template_name="mobile/login.html"    
    if request.method=="POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        ref      = request.POST.get("ref","/")
        if ref==None or ref=="" or "/login/" in ref: ref="/"
        user = authenticate(username=username, password=password)
        
        if user==None: 
            msg=u"Неверное имя пользователя или пароль"
            return render(request,template_name,locals())
        login(request,user)
        send_signal(1,u"Вошел пользователь %s"%(user.username))
        return HttpResponseRedirect(ref)
    else:
        if request.user.is_anonymous(): 
            ref = request.META.get("HTTP_REFERER")
            return render(request,template_name,locals())
        logout(request)
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

def register(request,template_name="register.html"):
    if isMobile(request): template_name="mobile/register.html"    
    if not request.user.is_anonymous(): return HttpResponseRedirect("/")
    if request.method=="POST":
        username  = request.POST.get("username")
        email     = request.POST.get("email")
        password1 = request.POST.get("password1")
        password2 = request.POST.get("password2")
        ref       = request.POST.get("ref","/")
        if ref=="" or "/login/" in ref or "/register/" in ref: ref="/"
        if ref==None: ref=receiver.POST.get("next")
        if ref==None: ref = "/"
        
        if password1!=password2: 
            msg=u"Введеные пароли не совпадают"
            return render(request,template_name,locals())
        if User.objects.filter(username__exact=username).count()>0: 
            msg=u"Пользователь с именем "+username+u" уже зарегистрирован"
            return render(request,template_name,locals())
        user = User.objects.create_user(username=username,password=password1,email=email)
        user.save()
        user = authenticate(username=username, password=password1)
        login(request,user)
        send_signal(1,u"Зарегистрировался пользователь %s"%(user.username))
        return HttpResponseRedirect(ref)
        
    ref = request.META.get("HTTP_REFERER")
    return render(request,template_name,locals())


def home(request,template_name="home.html"):
    if isMobile(request): template_name="mobile/home.html"    
    bs = Book.objects.all().order_by("-id")[:10]
    return render(request,template_name,locals())

def upload_book(request,template_name="upload_book.html"):
    
    if request.method=="POST":
        f = request.FILES.get("file")
        b = Book()
        b.user = request.user
        b.name = f.name.replace("/","")
        #b.author = request.POST.get("author")
        b.ru_title = request.POST.get("ru_name")
        b.en_title = request.POST.get("en_name")
        b.save()        
        os.mkdir(b.book_dir())      
        
        destination = open(b.book_file_fb2(), 'wb+')            
        for chunk in f.chunks():
            destination.write(chunk)                        
        destination.close()
        
    return render(request,template_name,locals())
    
def translate(request,template_name="translation.htm"):
    word = request.GET.get("t")
    ht = request.GET.get("ht") # нужно ли возвращать перевод
    #print "getting",word
    txt = ""
    if word!=None:
        w = Word.objects.filter(word=word.lower()).exclude(trans=None)
        if w.count()==0:
            txt = "Перевод отсутствует"
        else: 
            txt = w[0].trans
            ow  = w[0].one_word
            if not request.user.is_anonymous() and w[0].one_word!=None:
                Learn.objects.get_or_create(user=request.user,word=word.lower(),answer=w[0].one_word)
    
    if ht!=None: return HttpResponse('ok')
    
    return render_to_response(template_name,locals())
    
def book(request,uid,page=0,template_name="book.html"):
    if isMobile(request): template_name="mobile/book.html"    
    page = int(page)
    
    bk = Book.objects.get(uid=uid)
    
    if request.user.is_authenticated(): 
        lr,cr = LastRead.objects.get_or_create(user=request.user,book=bk)
        lr.dt = datetime.datetime.now()
        lr.save()
    
    if request.user.is_authenticated() and page==0 and request.GET.get("p")==None:
            up = UserPage.objects.filter(user=request.user,book=bk)
            if up.count()==0:
                page = 0
            else:
                page = up[0].page
    
    
    fname = bk.book_file_html(page)
    if not os.path.exists(fname):        
        if page==0 and not os.path.exists(bk.book_file_htm()): 
            parseFB2(bk)
        if os.path.exists(bk.book_file_htm(page)):
            translateFile(bk,page,bk.book_file_htm(page),fname)
    
    url = bk.content_url(page)
    translation_url = reverse('book_translation',args=[bk.uid,page])
    if isMobile(request):
        translation_url = bk.translation_url(page)
    
    
    if os.path.exists(bk.book_file_htm(page+1)):
        next_page = page+1
    
    if not request.user.is_anonymous():
        up,cr = UserPage.objects.get_or_create(user=request.user,book=bk)
        up.page = page
        up.save()
    prev_page = None
    if page>0:
        prev_page = page-1
        
    return render(request,template_name,locals())
    
def rebuildTranslation(request,book,page):
    html = ""
    t = loader.get_template('translation.htm')
    for wb in WordInBook.objects.filter(book=book,chapter=page):
        word = wb.word
        w = Word.objects.filter(word=word.lower()).exclude(trans=None)
        if w.count()==0:
            txt = "Перевод отсутствует"
        else: 
            txt = w[0].trans
            ow  = w[0].one_word
            if not request.user.is_anonymous() and w[0].one_word!=None:
                Learn.objects.get_or_create(user=request.user,word=word.lower(),answer=w[0].one_word)
        html += "<trans word='%s'>"%(word)
        c = Context(locals())
        html += t.render(c)
        html += "</trans>"
    f = file(book.translation_file(page),"w")
    f.write(html.encode('utf-8'))
    f.close()
    
def bookTranslation(request,uid,page=0):
    page = int(page)
    book = Book.objects.get(uid=uid)
    
    if not os.path.exists(book.translation_file(page)):
        rebuildTranslation(request,book,page)
        
    return HttpResponseRedirect(book.translation_url(page))
    
@login_required    
def learn(request,bid=0,chapter=0,template_name="learn.html"):
    if isMobile(request): template_name="mobile/learn.html"    
    if bid!=0 and chapter!=0:
        for wb in WordInBook.objects.filter(book_id=bid,chapter=chapter):
            ln,cr = Learn.objects.get_or_create(user=request.user,word=wb.word)
            if cr:
                ln.answer = Word.objects.get(word=wb.word).one_word
                ln.save()
    
    if request.is_ajax():
        template_name="learn_word.htm"
    
    Learn.objects.filter(answer=None).delete()
    
    lns = Learn.objects.filter(user=request.user,acnt__lt=4).order_by("?")
    if lns.count()==0: return HttpResponseRedirect("/")
    xword = None
    words = []
    for d in lns:
        if xword==None: xword=d
        words.append(d.answer.lower())
        if len(words)==4: break
        
    if len(words)<4:
        for d in Word.objects.all().exclude(word__in=words).order_by("?")[:4]:
            words.append(d.one_word.lower())
            if len(words)==4: break
    
    random.shuffle(words)
    word = Word.objects.get(word=xword.word)
    words_count = Learn.objects.filter(user=request.user,acnt__gte=4).count()
    return render(request,template_name,locals())
    
def checkWord(request,w1,w2):
    cnt = Word.objects.filter(word__iexact=w1,one_word__iexact=w2).count()
    
    if request.user.is_authenticated():
        ln = Learn.objects.filter(user=request.user,word=w1)
        if ln.count()>0:
            ln = ln[0]
            if cnt==1: 
                ln.acnt += 1
            else:
                ln.acnt = 0
            ln.save()
        
    return HttpResponse(str(cnt))

@login_required
def removeBookmark(request,id):
    LastRead.objects.filter(user=request.user,pk=id).delete()
    return HttpResponseRedirect("/")
    
def booksUser(request,uid,template_name="books_user.html"):
    u = User.objects.get(username=uid)
    bs = Book.objects.filter(user=u).order_by("-id")[:100]
    return render(request,template_name,locals())
    
def booksGenre(request,uid,template_name="books_genre.html"):
    data = Genre.objects.get(uid=uid)
    bs = Book.objects.filter(genre=data).order_by("-id")[:100]
    return render(request,template_name,locals())
    
def booksSeries(request,uid,template_name="books_genre.html"):
    data = Series.objects.get(uid=uid)
    bs = Book.objects.filter(series=data).order_by("-id")[:100]
    return render(request,template_name,locals())
    
def booksAuthor(request,uid,template_name="books_genre.html"):
    data = Author.objects.get(uid=uid)
    bs = Book.objects.filter(author=data).order_by("-id")[:100]
    return render(request,template_name,locals())

def news(request,template_name="news.html"):
    news = News.objects.all()[:50]
    return render(request,template_name,locals())
    
def readTopic(request,id,template_name="read_news.html"):
    topic = News.objects.get(pk=id)
    
    url = topic.trans_url()
    #translation_url = reverse('book_translation',args=[bk.uid,page])
    #if isMobile(request):
    #    translation_url = bk.translation_url(page)

    return render(request,template_name,locals())