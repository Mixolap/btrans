# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.conf import settings

#from models import BookInfo
try:
    from Ft.Xml.Xslt import Processor
    from Ft.Xml import InputSource
    from Ft.Xml.Domlette import NonvalidatingReader
except:
    print "NO XML"
    
from zipfile import ZipFile

from bs4 import BeautifulSoup

import os
import datetime
import re

##кеширует fb2 книгу
def mkFb2Cache(cache_name, fb2str):
    fb2str = royallibfix(fb2str) #FIX для книг с royallib.ru
    xsl = open(os.path.join(settings.BASE_DIR,"static/fb2html/book.xsl"), "r")
    processor = Processor.Processor()
    transform = InputSource.DefaultFactory.fromString(xsl.read(), "http://spam.com/identity.xslt")
    source = InputSource.DefaultFactory.fromString(fb2str, "http://spam.com/doc.xml")
        
    processor.appendStylesheet(transform)
        
    cache = open(cache_name, 'w')
    cache.write(processor.run(source))
        
    cache.close()
    xsl.close()
    return cache

##возвращает жанр по его имени. если нет - создает новый
def genreByName(name):    
    if Genre.objects.filter(name=name).count() > 0:
        genre = Genre.objects.filter(name=name)[0]
    else:
        genre = Genre(name=name, display_name=name)
        genre.save()    
    return genre

##возвращает автора по его uuid или None
def authorByUUID(uuid):    
    if Author.objects.filter(uuid=uuid).count() > 0:
        return Author.objects.filter(uuid=uuid)[0]
    else:
        return None

##возвращает переводчика по его uuid или None
def translatorByUUID(uuid):    
    if Translator.objects.filter(uuid=uuid).count() > 0:
        return Translator.objects.filter(uuid=uuid)[0]
    else:
        return None
## в книгах royallib.ru в последней строке есть href== из-за него парсер выдает ошибку. фиксим
def royallibfix(fb2str):
    return fb2str.replace('href==', 'href=').replace('RoyalLib.ru','').replace('royallib.ru', '')

##достаёт из файла fb2 разметку в виде строки
def extractFb2str(file):
    fb2str = ''
    if file.ext() == '.fb2.zip':
        try:
            zip = ZipFile(file.filepath(), 'r')        
            for name in zip.namelist():
                if os.path.splitext(name)[1]=='.fb2':
                    fb2str = zip.read(name)
                    break
            zip.close()
        except: 
            fb2str = open(file.filepath(), 'r').read()
    else:
        fb2str = open(file.filepath(), 'r').read()
        
    fb2str = royallibfix(fb2str) #FIX для книг с royallib.ru    
    return fb2str
    
##парсит Fb2 разметку, возвращает документ
def parseFb2(fb2str):
    xsl = open(os.path.join(settings.BASE_DIR,"static/fb2html/book-meta.xsl"), "r")
    processor = Processor.Processor()
    transform = InputSource.DefaultFactory.fromString(xsl.read(), "http://spam.com/identity.xslt")
    source = InputSource.DefaultFactory.fromString(fb2str, "http://spam.com/doc.xml")
        
    processor.appendStylesheet(transform)
    cache = processor.run(source)
    xsl.close()

    return NonvalidatingReader.parseString(cache)
  
##инфо о fb2книге    
def fb2Info(file):
    if not os.path.exists(file.filepath()): return None
    infos = BookInfo.objects.filter(file=file)
    if infos.count() > 0: 
        bookinfo = infos[0]
    else:       
        fb2str = extractFb2str(file)
        doc =  parseFb2(fb2str)
        
        bookinfo = BookInfo(file=file, title=file.display_name, annotation='', dt=datetime.datetime.now())
        bookinfo.save()
        
        for node in doc.firstChild.childNodes:
            if node.nodeName == u'authors':
                for xmlauthor in node.childNodes:
                    uuid = xmlauthor.attributes[(None, 'uuid')].value
                    first_name = xmlauthor.attributes[(None, 'first-name')].value
                    last_name = xmlauthor.attributes[(None, 'last-name')].value
                    middle_name = xmlauthor.attributes[(None, 'middle-name')].value
                    if uuid != u'': #пробуем найти автора по uuid
                        author = authorByUUID(uuid)
                        if author == None: #указан uuid - создаем нового #FIXME искать среди существующих авторов с такими же имя-фамилия и отутствием uuid
                            author = Author(uuid=uuid, first_name=first_name, last_name=last_name, middle_name=middle_name)
                            author.save()
                    else: #пробуем найти по фамилии - имени
                        author = Author.objects.filter(first_name=first_name, last_name=last_name)
                        if author.count() > 0: author = author[0]
                        else: #есть имя и фамилия - создаем нового
                            author = Author(uuid='', first_name=first_name, last_name=last_name, middle_name=middle_name)
                            author.save()
                    bookinfo.author.add(author)
                        
            if node.nodeName == u'genres':
                for xmlgenre in node.childNodes:
                    genre = genreByName(xmlgenre.attributes[(None, u'name')].value)
                    bookinfo.genre.add(genre)
                    
            if node.nodeName == u'translators':
                for xmltranslator in node.childNodes:
                    uuid = xmltranslator.attributes[(None, 'uuid')].value
                    first_name = xmltranslator.attributes[(None, 'first-name')].value
                    last_name = xmltranslator.attributes[(None, 'last-name')].value
                    middle_name = xmltranslator.attributes[(None, 'middle-name')].value
                    if uuid != u'': #пробуем найти переводчика по uuid
                        translator = authorByUUID(uuid)
                        if translator == None: #указан uuid - создаем нового #FIXME искать среди существующих переводчиков с такими же имя-фамилия и отутствием uuid
                            translator = Translator(uuid=uuid, first_name=first_name, last_name=last_name, middle_name=middle_name)
                            translator.save()
                    else: #пробуем найти по фамилии - имени
                        translator = Translator.objects.filter(first_name=first_name, last_name=last_name)
                        if translator.count() > 0: translator = translator[0]
                        else: #есть имя и фамилия - создаем нового
                            translator = Translator(uuid='', first_name=first_name, last_name=last_name, middle_name=middle_name)
                            translator.save()
                    bookinfo.translator.add(translator)
                
            if node.nodeName == u'annotation' and node.firstChild:
                bookinfo.annotation = node.firstChild.nodeValue
                
            if node.nodeName == u'title' and node.firstChild.nodeValue != u'':
                bookinfo.title = node.firstChild.nodeValue
                
        bookinfo.save()
    return bookinfo

def makeHTM(src,dst):    
    ctx = []
    
    cache_name = dst
    
    if os.path.exists(cache_name):
        cache = open(cache_name, "r")
        html = cache.read()
        cache.close()
    else:
        """
        fb2file = ""
        
        if file.ext() == '.fb2.zip':
            try:
                zip = ZipFile(file.filepath(), "r")
            
                for name in zip.namelist():
                    if os.path.splitext(name)[1]==".fb2":
                        fb2file = zip.read(name)
                        break
            except:
                fb2file = open(file.filepath(), "r").read()
        else:
        """
        fb2file = open(src, "r").read()
        mkFb2Cache(cache_name, fb2file)
        cache = open(cache_name, "r")
        html = cache.read()
        cache.close()        
    return html
    #return render_to_response(template_name, {'html':html, 'title':file.display_name}, context_instance=RequestContext(request))

##возвращает картинку с обложкой
def cover(request, id):
    file = get_object_or_404(Files,pk=id)   
    bookinfo = fb2Info(file)
    if bookinfo.cover:
        covername = os.path.join(EBOOKS_CACHE_PATH, '%d_cover.jpg'%file.id)
        res = HttpResponse(mimetype="image/jpg",content_type='image/jpeg')
    else:
        covername = os.path.join(MEDIA_ROOT, 'img', 'book_big.png')
        res = HttpResponse(mimetype="image/png",content_type='image/png')
        
    if 'w' in request.GET or 'h' in request.GET:
        import Image
        im = Image.open(covername)
        im.thumbnail((
            int(request.GET.get("w",
                    int(request.GET.get("h",10))
                )
            ), 
            int(request.GET.get("h",
                    int(request.GET.get("w",10))
                )
            )
        ))
        im.save(res, "PNG")        
    else:
        res.write(open(covername, 'rb').read())        
    res["Accept-Ranges"]="bytes"
    return res
    
##меняет display_name файла на title книги    
def normFB2File(file):
    from interest.inttypes import InterestTypeFactory, EBookInterestType
    itf = InterestTypeFactory() 
    if isinstance(itf.detectFileType(file), EBookInterestType):  
        info = fb2Info(file)
        authors = info.author.all()
        authors = ['%s %s'%(a.first_name, a.last_name) for a in authors]
        authors = ', '.join(authors)
        file.display_name = '%s - %s'%(authors, info.title)
        file.save()
def myEscape(text):
    text = text.replace()

def parsePerPages(fname):
    outname = fname.split(".")
    del outname[-1]
    outname = ".".join(outname)
    f    = open(fname,'r')
    data = f.read()
    f.close()
    bs   = BeautifulSoup(data)
    page = 1  
    text = ""
    p1 = re.compile(r'<a.*?>|</a>')
    
    xlen = 50000
    
    h1_count = len(bs.find_all("h1"))
    h2_count = len(bs.find_all("h2"))
    
    if h1_count>h2_count:
        for tag in bs.body.children:
            if tag.name!=None and tag.name.lower() in ["h1","h2","h3"]:      
                print len(text)
                if len(text)<xlen:
                    text += tag.encode('utf-8')
                else:
                    f = open(outname+"_"+str(page)+".htm","w")
                    
                    text = p1.sub('',text)                                
                    
                    f.write(text)
                    f.close()
                    page += 1
                    xlen = 5000
                    text = tag.encode('utf-8')
            else:
                text += tag.encode('utf-8')
    else:
        for tag in bs.body.children:
            #print tag.name
            #if tag.name!=None and tag.name=="div"
            
            if tag.name!=None and tag.name.lower() in ["h1","h2","h3"]:      
                print len(text)
                if len(text)<xlen:
                    text += tag.encode('utf-8')
                else:
                    f = open(outname+"_"+str(page)+".htm","w")
                    
                    text = p1.sub('',text)                                
                    
                    f.write(text)
                    f.close()
                    page += 1
                    xlen = 5000
                    text = tag.encode('utf-8')
            else:
                text += tag.encode('utf-8')
                
    f = open(outname+"_"+str(page)+".htm","w")
    f.write(text)
    f.close()
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    