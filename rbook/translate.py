# -*- encoding: utf-8 -*-

from models import *
from BeautifulSoup import BeautifulSoup
import re


def translateFile(book,chapter,src,dst):
    print "translating",src
    f = open(src,"r")
    data = f.read().decode('utf-8')
    f.close()
    soup  = BeautifulSoup(data)
    xdata = ''.join(soup.findAll(text=True))    
    #print xdata
    founds = []
    for m in re.findall("(\w+)",xdata):
        if len(m)==1: continue
        if not m.isalpha(): continue
        xm = m.lower()
        if xm in ["nbsp","rsquo","ldquo","rdquo","mdash","hellip"]: continue        
        if m in founds: continue
        founds.append(m)
        wb,cr = Word.objects.get_or_create(word=xm)
        if cr:
            wb.book = book
            wb.save()
        if book!=None:
            WordInBook.objects.get_or_create(book=book,chapter=chapter,word=xm)
        #print m
        data = re.sub(r'([>,\.!?\s;:&])'+m+r'([&<,\.!?\s;:])',r"\1<x>"+m+r"</x>\2",data)
 
    f = open(dst,"w")
    f.write(data.encode('utf-8'))
    f.close()
