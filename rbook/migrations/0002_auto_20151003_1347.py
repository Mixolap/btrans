# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rbook', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='is_downloaded',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='news',
            name='is_processed',
            field=models.BooleanField(default=False),
        ),
    ]
