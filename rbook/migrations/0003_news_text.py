# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rbook', '0002_auto_20151003_1347'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='text',
            field=models.TextField(null=True, blank=True),
        ),
    ]
