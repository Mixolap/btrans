# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': '\u0410\u0432\u0442\u043e\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('en_title', models.CharField(max_length=255, null=True, blank=True)),
                ('ru_title', models.CharField(max_length=255, null=True, blank=True)),
                ('name', models.CharField(max_length=255)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
                ('author', models.ForeignKey(blank=True, to='rbook.Author', null=True)),
            ],
            options={
                'verbose_name_plural': '\u041a\u043d\u0438\u0433\u0438',
            },
        ),
        migrations.CreateModel(
            name='Feed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('href', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': '\u0416\u0430\u043d\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='LastRead',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('book', models.ForeignKey(to='rbook.Book')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Learn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('word', models.CharField(max_length=255)),
                ('acnt', models.IntegerField(default=0)),
                ('answer', models.CharField(max_length=255)),
                ('dt', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=500)),
                ('href', models.CharField(max_length=255, null=True, blank=True)),
                ('dt', models.DateTimeField()),
                ('source', models.ForeignKey(to='rbook.Feed')),
            ],
        ),
        migrations.CreateModel(
            name='Series',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('uid', models.CharField(max_length=255, null=True, blank=True)),
                ('author', models.ForeignKey(to='rbook.Author')),
            ],
            options={
                'verbose_name_plural': '\u0421\u0435\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='UserPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page', models.IntegerField(default=1)),
                ('book', models.ForeignKey(to='rbook.Book')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Word',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lang', models.IntegerField(default=1)),
                ('word', models.CharField(max_length=255)),
                ('trans', models.TextField(null=True, blank=True)),
                ('one_word', models.CharField(max_length=255)),
                ('texts', models.TextField(null=True, blank=True)),
                ('from_book', models.ForeignKey(blank=True, to='rbook.Book', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='WordInBook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('chapter', models.IntegerField(verbose_name='\u0413\u043b\u0430\u0432\u0430')),
                ('word', models.CharField(max_length=255)),
                ('book', models.ForeignKey(to='rbook.Book')),
            ],
        ),
        migrations.AddField(
            model_name='book',
            name='genre',
            field=models.ForeignKey(blank=True, to='rbook.Genre', null=True),
        ),
        migrations.AddField(
            model_name='book',
            name='series',
            field=models.ForeignKey(blank=True, to='rbook.Series', null=True),
        ),
        migrations.AddField(
            model_name='book',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
