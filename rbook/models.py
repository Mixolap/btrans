#-*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib import admin

import os


def isMobile(request):
    ua = request.META.get("HTTP_USER_AGENT").lower()
    mobiles = ["android","iphone","ipad","windows mobile"]
    for m in mobiles:
        if m in ua: return True
    return False

def ecran(s):
    s1 = [u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u"0",u"«",u"»",u"/",u"—",u'"',u"'",u']',u'[',u'?',u'+',u'•',u'#',u'–',u'а',u'б',u'в',u'г',u'д',u'е',u'ё',u'ж',u'з',u'и',u'й',u'к',u'л',u'м',u'н',u'о',u'п',u'р',u'с',u'т',u'у',u'ф',u'х',u'ц',u'ч',u'ш',u'щ',u'ъ',u'ы',u'ь',u'э',u'ю',u'я']
    s2 = [u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u"0",u" ",u" ",u" ",u" ",u" ",u" ",u' ',u' ',u' ',u' ',u'-',u'_','-','a','b','v','g','d','e','yo','j','z','i','i','k','l','m','n','o','p','r','s','t','u','f','x','ts','ch','sh','sh','','y','','e','y','ia','h','c','w','q']
    for i in xrange(len(s1)):        
        s = s.replace(s1[i],s2[i])
        s = s.replace(s1[i].upper(),s2[i].upper())      
    for i in xrange(len(s)):
        if s[i] in [".","-"]: continue        
        if s[i].lower() not in s2: 
            s=s.replace(s[i],"_")            
            continue
        if not (s[i].isalpha() or s[i].isnumeric()): s=s.replace(s[i],"_")
    return s

class Genre(models.Model):
    name = models.CharField(max_length=255)
    uid  = models.CharField(max_length=255,null=True,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u"Жанры"
        
    def save(self, *args, **kwargs):
        if not self.uid:            
            link = ecran(self.name.replace(".fb2","")).replace(".","_").replace(" ","_").replace("-","").replace("__","_").lower()
            if Genre.objects.filter(uid=link).count()>0:
                if self.id!=None: 
                    link = str(self.id)+'_'+link
                else:
                    lastid=Genre.objects.latest('id')
                    link = str(lastid.id+1)+'_'+link
            self.uid = link
        super(Genre, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None: self.save()
        return "/books/genre/%s/"%self.uid

class Author(models.Model):
    name = models.CharField(max_length=255)
    uid  = models.CharField(max_length=255,null=True,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u"Авторы"
    def save(self, *args, **kwargs):
        if not self.uid:            
            link = ecran(self.name.replace("fb2","")).replace(".","_").replace(" ","_").replace("-","").replace("__","_").lower()
            if Author.objects.filter(uid=link).count()>0:
                if self.id!=None: 
                    link = str(self.id)+'_'+link
                else:
                    lastid=Author.objects.latest('id')
                    link = str(lastid.id+1)+'_'+link
            self.uid = link
        super(Author, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None: self.save()
        return "/books/author/%s/"%self.uid
        
class Series(models.Model):
    author = models.ForeignKey(Author)
    name   = models.CharField(max_length=255)
    uid    = models.CharField(max_length=255,null=True,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = u"Серии"
    def save(self, *args, **kwargs):
        if not self.uid:            
            link = ecran(self.name.replace("fb2","")).replace(".","_").replace(" ","_").replace("-","").replace("__","_").lower()
            if Series.objects.filter(uid=link).count()>0:
                if self.id!=None: 
                    link = str(self.id)+'_'+link
                else:
                    lastid=Series.objects.latest('id')
                    link = str(lastid.id+1)+'_'+link
            self.uid = link
        super(Series, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None: self.save()
        return "/books/series/%s/"%self.uid
        
class Book(models.Model):
    user       = models.ForeignKey(User)
    author     = models.CharField(max_length=255,null=True,blank=True)
    en_title   = models.CharField(max_length=255,null=True,blank=True)
    ru_title   = models.CharField(max_length=255,null=True,blank=True)
    name       = models.CharField(max_length=255)
    dt         = models.DateTimeField(auto_now_add=True)
    uid        = models.CharField(max_length=255,null=True,blank=True)
    genre      = models.ForeignKey(Genre,null=True,blank=True)
    author     = models.ForeignKey(Author,null=True,blank=True)
    series     = models.ForeignKey(Series,null=True,blank=True)
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = u"Книги"
    
    def save(self, *args, **kwargs):
        if not self.uid:            
            link = ecran(self.name.replace("fb2","")).replace(".","_").replace(" ","_").replace("-","").replace("__","_").lower()
            if Book.objects.filter(uid=link).count()>0:
                if self.id!=None: 
                    link = str(self.id)+'_'+link
                else:
                    lastid=Book.objects.latest('id')
                    link = str(lastid.id+1)+'_'+link
            self.uid = link
        super(Book, self).save(*args, **kwargs)
        
    def get_absolute_url(self):
        if self.uid==None: self.save()
        return "/book/%s/"%self.uid
    def get_page_url(self,page):
        return "/book/%s/%s/"%(self.uid,str(page))
        
    def book_dir(self):
        return os.path.join(settings.BOOKS_DIR,str(self.id))
        
    def book_file_fb2(self):
        return os.path.join(self.book_dir(),self.name)
        
    def book_file_htm(self,page=0):
        if page!=0:
            return os.path.join(self.book_dir(),self.name+"_"+str(page)+".htm")
        return os.path.join(self.book_dir(),self.name+".htm")

    def book_file_html(self,page):
        return os.path.join(self.book_dir(),self.name+"_"+str(page)+".html")
        
    def translation_file(self,page):
        return os.path.join(self.book_dir(),"trans_"+self.name+"_"+str(page)+".html")
        
    def content_url(self,page):
        return "/static/books/"+str(self.id)+"/"+self.name+"_"+str(page)+".html"
        
    def translation_url(self,page):
        return "/static/books/"+str(self.id)+"/trans_"+self.name+"_"+str(page)+".html"
        
    def img_path(self):
        return os.path.join(self.book_dir(),self.name+".jpg")
        
    def img_url(self):
        return "/static/books/"+str(self.id)+"/"+self.name+".jpg"
        

class Word(models.Model):
    lang  = models.IntegerField(default=1) # 1 - английский
    word  = models.CharField(max_length=255)    
    trans = models.TextField(null=True,blank=True)
    one_word = models.CharField(max_length=255)
    from_book = models.ForeignKey(Book,null=True,blank=True)
    texts = models.TextField(null=True,blank=True)

class Learn(models.Model):
    user    = models.ForeignKey(User)
    word    = models.CharField(max_length=255)
    acnt    = models.IntegerField(default=0)
    answer  = models.CharField(max_length=255) 
    dt      = models.DateTimeField(auto_now=True)
    
    
class WordInBook(models.Model):
    book    = models.ForeignKey(Book)
    chapter = models.IntegerField(verbose_name=u"Глава")
    word    = models.CharField(max_length=255)
    
    
class UserPage(models.Model):
    user    = models.ForeignKey(User)
    book    = models.ForeignKey(Book)
    page    = models.IntegerField(default=1)

class LastRead(models.Model):
    user = models.ForeignKey(User)
    book = models.ForeignKey(Book)
    dt   = models.DateTimeField(auto_now_add=True)


class Feed(models.Model):
    title = models.CharField(max_length=255)
    href  = models.CharField(max_length=255)
    def __unicode__(self):
        return self.title
        
class News(models.Model):
    source = models.ForeignKey(Feed)
    title  = models.CharField(max_length=500)
    href   = models.CharField(max_length=255,null=True,blank=True)
    text   = models.TextField(null=True,blank=True)
    dt     = models.DateTimeField()
    is_downloaded = models.BooleanField(default=False)
    is_processed  = models.BooleanField(default=False)
    
    def __unicode__(self):
        return self.title
    class Meta:
        ordering = ["-dt",]
    def source_file_name(self):
        return os.path.join(self.text_dir(),"source.html")
        
    def trans_file_name(self):
        return os.path.join(self.text_dir(),"trans.html")
        
    def trans_url(self):
        return "/static/news/%d/trans.html"%self.id
    def text_dir(self):
        return os.path.join(settings.NEWS_DIR,str(self.id))
    
    
admin.site.register(Book)    
admin.site.register(Word)    
admin.site.register(Genre)    
admin.site.register(Author)    
admin.site.register(Series)    
admin.site.register(Feed)    
admin.site.register(News)    
