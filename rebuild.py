#-*- encoding: utf-8 -*-
import re,os,urllib2,time,random
from bs4 import BeautifulSoup
from xml.dom.minidom import Document
import django,os,json,re,subprocess
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "btrans.settings")
from rbook.models import *
from rbook.parse_fb2 import parseFB2
from rbook.translate import translateFile
from django.conf import settings
# пересоздания кеша книг


def rebuildCache():
    subprocess.check_output(["find",settings.BOOKS_DIR,"-name","*.htm*","-delete"])

    total_time = time.time()
    for bk in Book.objects.all():
        print "parsing book",bk.name
        fname = bk.book_file_html(0)
        time1 = time.time()
        if not os.path.exists(fname): 
            parseFB2(bk)
            translateFile(bk,0,bk.book_file_htm(0),fname)
        page=1    
        while os.path.exists(bk.book_file_htm(page)):
            print bk.book_file_htm(page),"translating",page,"chapter"
            time2 = time.time()
            translateFile(bk,page,bk.book_file_htm(page),bk.book_file_html(page))
            print "ttime=",time.time()-time2
            page += 1 
        print "time parsing book=",time.time()-time1
        
    print "total_time",time.time()-total_time

rebuildCache()

#words = ['beyond','constrictor','reassured','scowled','grown']    
for w in Word.objects.filter(texts=None).order_by("word"):       
    word = w.word
    print "WORD=",word
    x = []
    for bk in Book.objects.all():
        fname = bk.book_file_fb2()
        f = open(fname,"r")
        try:
            data = f.read().decode('utf-8').replace("</p>",".").replace("<p>","").replace("<emphasis>","").replace("</emphasis>","").replace("<title>","").replace("</title>","")
        except:
            continue
        f.close()        
        for d in data.split("."):
            d = d.strip()
            if len(d)>150: continue
            if len(d)<40: continue
            if word.lower() in d.lower():
                x.append(d+".")
                break
        if len(x)>2: break
    w.texts = "\n".join(x)
    w.save()
    for d in x:
        print d

    
    
    